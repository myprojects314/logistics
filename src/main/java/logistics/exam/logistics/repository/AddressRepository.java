package logistics.exam.logistics.repository;


import logistics.exam.logistics.model.Address;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {
    @EntityGraph(attributePaths = {"managedAddresses", "manager"})
    Optional<Address> findByUsername(String username);
}
