package logistics.exam.logistics.security;

import logistics.exam.logistics.model.Address;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class LogisticsUser extends User {
    private Address address;

    public LogisticsUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Address address) {
        super(username, password, authorities);
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
