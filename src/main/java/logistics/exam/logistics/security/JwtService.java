package logistics.exam.logistics.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import jakarta.annotation.PostConstruct;
import logistics.exam.logistics.config.LogisticsConfigProperties;
import logistics.exam.logistics.model.Address;
import com.auth0.jwt.JWTCreator.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.*;

@Service
public class JwtService {
    private static final String MANAGER = "manager";
    private static final String MANAGED_ADDRESSES = "managedAddresses";
    private static final String USERNAME = "username";
    private static final String ID = "id";
    private Algorithm algorithm;

    @Autowired
    private LogisticsConfigProperties config;

    @PostConstruct
    public void init() {
        LogisticsConfigProperties.JwtData jwtConfig = config.getJwtData();
        try {
            Method method = Algorithm.class.getMethod(jwtConfig.getAlg(), String.class);
            algorithm = (Algorithm) method.invoke(Algorithm.class, jwtConfig.getSecret());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String createJwt(UserDetails userDetails) {

        LogisticsConfigProperties.JwtData jwtConfig = config.getJwtData();

        Address address = ((LogisticsUser) userDetails).getAddress();

        Builder jwtBuilder = (Builder) JWT.create()
                .withSubject(userDetails.getUsername())
                .withArrayClaim("auth", userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).toArray(String[]::new))
                .withClaim(ID, address.getId());

        Address manager = address.getManager();
        if (manager != null) {
            jwtBuilder.withClaim(MANAGER, createMapFromAddress(manager));
        }

        List<Address> managedAddresses = address.getManagedAddress();
        if (managedAddresses != null && !managedAddresses.isEmpty()) {
            jwtBuilder.withClaim(MANAGED_ADDRESSES,
                    managedAddresses.stream().map(this::createMapFromAddress).toList());
        }

        return jwtBuilder
                .withExpiresAt(new Date(System.currentTimeMillis() + jwtConfig.getDuration().toMillis()))
                .withIssuer(jwtConfig.getIssuer())
                .sign(algorithm);
    }

    private Map<String, Object> createMapFromAddress(Address address) {
        return Map.of(
                ID, address.getId(),
                USERNAME, address.getUsername()
        );
    }

    public UserDetails parseJwt(String jwtToken) {

        DecodedJWT decodedJwt = JWT.require(algorithm)
                .withIssuer(config.getJwtData().getIssuer())
                .build()
                .verify(jwtToken);

        Address address = new Address();
        address.setId(decodedJwt.getClaim(ID).asLong());
        address.setUsername(decodedJwt.getSubject());

        Claim managerClaim = decodedJwt.getClaim(MANAGER);

        Map<String, Object> managerData = managerClaim.asMap();
        address.setManager(parseAddressFromMap(managerData));

        Claim managedAddressClaim = decodedJwt.getClaim(MANAGED_ADDRESSES);
        address.setManagedAddress(new ArrayList<>());
        List<HashMap> managedAddressesMaps = managedAddressClaim.asList(HashMap.class);
        if (managedAddressesMaps != null) {
            for (var addressMap : managedAddressesMaps) {
                Address managedAddress = parseAddressFromMap(addressMap);
                if (managedAddress != null)
                    address.getManagedAddress().add(managedAddress);
            }
        }

        return new LogisticsUser(decodedJwt.getSubject(), "dummy", decodedJwt.getClaim("auth").asList(String.class).stream()
                .map(SimpleGrantedAuthority::new).toList(), address);
    }

    private Address parseAddressFromMap(Map<String, Object> addressMap) {
        if (addressMap != null) {
            Address address = new Address();
            address.setId(((Integer) addressMap.get(ID)).longValue());
            address.setUsername((String) addressMap.get(USERNAME));
        }
        return null;
    }

}
