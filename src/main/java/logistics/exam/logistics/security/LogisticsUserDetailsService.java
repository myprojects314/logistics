package logistics.exam.logistics.security;

import logistics.exam.logistics.model.Address;
import logistics.exam.logistics.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class LogisticsUserDetailsService implements UserDetailsService {
    @Autowired
    AddressRepository addressRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Address address = addressRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        return new LogisticsUser(username, address.getPassword(), Arrays.asList(new SimpleGrantedAuthority("USER")), address);
    }
}
