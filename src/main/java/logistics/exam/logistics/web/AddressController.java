package logistics.exam.logistics.web;

import logistics.exam.logistics.dto.AddressDto;
import logistics.exam.logistics.mapper.AddressMapper;
import logistics.exam.logistics.model.Address;
import logistics.exam.logistics.repository.AddressRepository;
import logistics.exam.logistics.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/addresses")
public class AddressController {

    @Autowired
    AddressMapper addressMapper;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    AddressService addressService;

    @GetMapping
    public List<AddressDto> findAllAddress() {
        List<Address> addressList = addressRepository.findAll();
        return addressMapper.addressToDtoList(addressList);
    }

    @GetMapping("/{id}")
    public AddressDto findAddress(@RequestParam long id) {
        return addressMapper.addressToDto(getAddressOrThrow(id));
    }

    @PostMapping
    public AddressDto createAddress(@RequestBody AddressDto addressDto) {
        return addressMapper.addressToDto(addressRepository.save(addressMapper.dtoToAddress(addressDto)));
    }

    @PutMapping("/{id}")
    public AddressDto updateAddress(@RequestParam long id, @RequestBody AddressDto addressDto) {
        if (!addressRepository.existsById(id))
            return null;
        return addressMapper.addressToDto(addressRepository.save(addressMapper.dtoToAddress(addressDto)));
    }

    @PostMapping("/search")
    public List<AddressDto> searchAddress(@RequestBody AddressDto addressDto) {
        return addressMapper.addressToDtoList(addressService.findAddressesByExample(addressMapper.dtoToAddress(addressDto)));

    }

    @DeleteMapping("/{id}")
    public void deleteAddress(@RequestParam long id) {
        addressRepository.deleteById(id);
    }

    private Address getAddressOrThrow(long id) {
        return addressService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
