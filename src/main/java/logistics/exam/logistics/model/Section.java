package logistics.exam.logistics.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Section {
    @Id
    @GeneratedValue
    private long id;
    private int orderNumber;
    @ManyToOne
    private Milestone startMilestone;
    @ManyToOne
    private Milestone endMilestone;

    @ManyToOne
    private TransportPlan transportPlan;

    public Section(int orderNumber, Milestone startMilestone, Milestone endMilestone) {
        this.orderNumber = orderNumber;
        this.startMilestone = startMilestone;
        this.endMilestone = endMilestone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Milestone getStartMilestone() {
        return startMilestone;
    }

    public void setStartMilestone(Milestone startMilestone) {
        this.startMilestone = startMilestone;
    }

    public Milestone getEndMilestone() {
        return endMilestone;
    }

    public void setEndMilestone(Milestone endMilestone) {
        this.endMilestone = endMilestone;
    }
}
