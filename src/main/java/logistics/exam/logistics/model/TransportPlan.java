package logistics.exam.logistics.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

import java.util.List;

@Entity
public class TransportPlan {
    @Id
    @GeneratedValue
    private long id;
    private int income;

    @OneToMany(mappedBy = "transportPlan")
    private List<Section> sections;

    public TransportPlan(int income, List<Section> sections) {
        this.income = income;
        this.sections = sections;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
