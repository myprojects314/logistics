package logistics.exam.logistics.model;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Address {
    @Id
    @GeneratedValue
    private long id;
    private String isoCode;
    private String country;
    private String city;
    private String street;
    private int zipCode;
    private int houseNumber;
    private String latitude;
    private String longitude;

    @OneToMany(mappedBy = "address")
    private List<Milestone> milestone;

    @ManyToOne
    private Address manager;
    @OneToMany(mappedBy = "manager")
    private List<Address> managedAddress;
    private String username;
    private String password;

    public Address() {
    }

    public Address(String isoCode, String country, String city, String street,
                   int zipCode, int houseNumber, String latitude, String longitude) {
        this.isoCode = isoCode;
        this.country = country;
        this.city = city;
        this.street = street;
        this.zipCode = zipCode;
        this.houseNumber = houseNumber;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Milestone> getMilestone() {
        return milestone;
    }

    public void setMilestone(List<Milestone> milestone) {
        this.milestone = milestone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id == address.id && zipCode == address.zipCode && houseNumber == address.houseNumber
                && Objects.equals(isoCode, address.isoCode) && Objects.equals(city, address.city)
                && Objects.equals(street, address.street) && Objects.equals(latitude, address.latitude)
                && Objects.equals(longitude, address.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isoCode, city, street, zipCode, houseNumber, latitude, longitude);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getManager() {
        return manager;
    }

    public void setManager(Address manager) {
        this.manager = manager;
    }

    public List<Address> getManagedAddress() {
        return managedAddress;
    }

    public void setManagedAddress(List<Address> managedAddress) {
        this.managedAddress = managedAddress;
    }
}
