package logistics.exam.logistics.model;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Entity
public class Milestone {
    @Id
    @GeneratedValue
    private long id;
    private LocalDate plannedTime;
    @ManyToOne
    private Address address;

    public Milestone(LocalDate plannedTime) {
        this.plannedTime = plannedTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(LocalDate plannedTime) {
        this.plannedTime = plannedTime;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
