package logistics.exam.logistics.service;

import jakarta.transaction.Transactional;
import logistics.exam.logistics.model.Address;
import logistics.exam.logistics.model.TransportPlan;
import logistics.exam.logistics.repository.TransportPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;

import java.util.List;
import java.util.Optional;

public class TransportPlanService {

    @Autowired
    TransportPlanRepository transportPlanRepository;

    @Transactional
    public TransportPlan save(TransportPlan transportPlan) {
        return transportPlanRepository.save(transportPlan);
    }

    @Transactional
    public TransportPlan update(TransportPlan transportPlan) {
        if (!transportPlanRepository.existsById(transportPlan.getId()))
            return null;
        return transportPlanRepository.save(transportPlan);
    }

    public List<TransportPlan> findAll(@SortDefault("id") Pageable pageable) {
        Page<TransportPlan> transportPlanPage = transportPlanRepository.findAll(pageable);
        return transportPlanPage.getContent();
    }

    public Optional<TransportPlan> findById(long id) {
        return transportPlanRepository.findById(id);
    }

    @Transactional
    public void delete(long id) {
        transportPlanRepository.deleteById(id);
    }
}
