package logistics.exam.logistics.service;

import logistics.exam.logistics.model.Address;
import logistics.exam.logistics.model.Address_;
import org.springframework.data.jpa.domain.Specification;

public class AddressSpecifications {
    public static Specification<Address> hasId(long id) {
        return (root, cq, cb) -> cb.equal(root.get(Address_.ID), id);
    }

    public static Specification<Address> hasIsoCode(String country) {
        return (root, cq, cb) -> cb.equal(cb.lower(root.get(Address_.COUNTRY)), (country + "%").toLowerCase());
    }

    public static Specification<Address> hasCity(String city) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.CITY)), (city + "%").toLowerCase());
    }

    public static Specification<Address> hasZipCode(int zipCode) {
        return (root, cq, cb) -> cb.equal(root.get(Address_.ZIP_CODE), zipCode);
    }

    public static Specification<Address> hasStreet(String street) {
        return (root, cq, cb) -> cb.like(cb.lower(root.get(Address_.STREET)), (street + "%").toLowerCase());
    }
}
