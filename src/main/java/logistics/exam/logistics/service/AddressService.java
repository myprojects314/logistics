package logistics.exam.logistics.service;

import jakarta.transaction.Transactional;
import logistics.exam.logistics.model.Address;
import logistics.exam.logistics.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Transactional
    public Address save(Address address) {
        return addressRepository.save(address);
    }

    @Transactional
    public Address update(Address address) {
        if (!addressRepository.existsById(address.getId()))
            return null;
        return addressRepository.save(address);
    }

    public List<Address> findAll(@SortDefault("id") Pageable pageable) {
        Page<Address> addressPage = addressRepository.findAll(pageable);
        System.out.println(addressPage.getTotalElements());
        System.out.println(addressPage.isFirst());
        System.out.println(addressPage.isLast());
        System.out.println(addressPage.hasPrevious());
        System.out.println(addressPage.hasNext());
        return addressPage.getContent();
    }

    public Optional<Address> findById(long id) {
        return addressRepository.findById(id);
    }

    @Transactional
    public void delete(long id) {
        addressRepository.deleteById(id);
    }

    public List<Address> findAddressesByExample(Address addressExample) {
        long id = addressExample.getId();
        String isoCode = addressExample.getIsoCode();
        String city = addressExample.getCity();
        int zipCode = addressExample.getZipCode();
        String street = addressExample.getStreet();

        Specification<Address> spec = Specification.where(null);

        if (id > 0)
            spec = spec.and(AddressSpecifications.hasId(id));

        if (StringUtils.hasText(isoCode))
            spec = spec.and(AddressSpecifications.hasIsoCode(isoCode));

        if (StringUtils.hasText(city))
            spec = spec.and(AddressSpecifications.hasCity(city));

        if (zipCode > 0)
            spec = spec.and(AddressSpecifications.hasZipCode(zipCode));

        if (StringUtils.hasText(street))
            spec = spec.and(AddressSpecifications.hasStreet(street));

        return addressRepository.findAll(spec, Sort.by("id"));
    }
}
