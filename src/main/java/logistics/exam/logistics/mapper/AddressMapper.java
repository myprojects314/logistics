package logistics.exam.logistics.mapper;

import logistics.exam.logistics.dto.AddressDto;
import logistics.exam.logistics.model.Address;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    List<AddressDto> addressToDtoList(List<Address> addressList);

    @Mapping(target = "countryCode", source = "isoCode")
    AddressDto addressToDto(Address address);

    @InheritInverseConfiguration
    Address dtoToAddress(AddressDto addressDto);

    List<Address> dtoToAddressList(List<AddressDto> addressDtoList);
}
