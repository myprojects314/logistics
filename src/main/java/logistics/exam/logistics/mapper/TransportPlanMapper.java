package logistics.exam.logistics.mapper;

import logistics.exam.logistics.dto.TransportPlanDto;
import logistics.exam.logistics.model.TransportPlan;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransportPlanMapper {
    List<TransportPlanDto> transportPlanToDtoList(List<TransportPlan> transportPlanList);

    TransportPlanDto transportPlanToDto(TransportPlan transportPlan);

    TransportPlan dtoToTransportPlan(TransportPlanDto transportPlanDto);

    List<TransportPlan> dtoToTransportPlanList(List<TransportPlanDto> transportPlanDtoList);
}
